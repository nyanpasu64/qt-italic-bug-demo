#include "mainwindow.h"
#include "layout_macros.h"
#include "qfont_util.h"

#include <QApplication>
#include <QBoxLayout>
#include <QFontDialog>
#include <QLabel>
#include <QPushButton>

#include <iostream>
#include <stdexcept>
#include <QDebug>

#define release_assert(expr) \
    do { if (!(expr)) { \
        throw std::logic_error("release_assert failed: `" #expr "` is false"); \
    } } while (0)


void diff_fonts(QFont const & f1, QFont const & f2) {
#define DIFF(METHOD) \
    if (f1.METHOD() != f2.METHOD()) \
        qDebug() << #METHOD "() differs";
    DIFF(pointSize)
    DIFF(pixelSize)
    DIFF(weight)
    DIFF(style)
    DIFF(stretch)
    DIFF(styleHint)
    DIFF(styleStrategy)

    // not found in QFont::operator<, found in QFontDef::operator==
    // DIFF(ignorePitch) (not exposed to QFont)
    DIFF(fixedPitch)

    DIFF(families)
    DIFF(family)
    DIFF(capitalization)

    // not found in QFont::operator<, found in QFontDef::operator==
    DIFF(styleName)
    DIFF(hintingPreference)

    DIFF(letterSpacingType)
    DIFF(letterSpacing)
    DIFF(wordSpacing)

    DIFF(underline)
    DIFF(overline)
    DIFF(strikeOut)
    DIFF(kerning)
}

struct QFontPublic
{
    QExplicitlySharedDataPointer<QFontPrivate> d;
    uint resolve_mask;
};

// Casting QFont* to QFontPublic* is probably undefined behavior,
// but I don't really care.
uint get_resolve_mask(QFont & f) {
    return ((QFontPublic*)(&f))->resolve_mask;
}

static_assert(sizeof(QFont) == sizeof(QFontPublic), "");

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow wv;
    auto * w = &wv;

    // Main window, dx elements
    {add_central_widget(QWidget(parent), QHBoxLayout(w));

        // Left column, dy elements
        {append_layout(QVBoxLayout());
            {append_widget(QPushButton(parent));
                w->setText("Default QFont, not Source Sans Pro");
            }

    #define HEADING(text) qDebug() << "\n" "# " text;

            QFont set_bold_italic;
            {
                HEADING("toString()")
                // Varies depending on your DE font selections.
                qDebug() << set_bold_italic.toString();

                set_bold_italic.setFamily("Source Sans Pro");
                set_bold_italic.setPointSize(10);
                setAttributes(set_bold_italic, true, MaybeUnsigned{}, true, "");

                qDebug() << set_bold_italic.toString();
            }

            if (true) {
                HEADING("QFont{}")

            }

            if (true) {
                HEADING("Set custom QWidget font, get back inherited font")
                qDebug() << "QFont{} = " << QFont{};

                auto * parent = w;
                qDebug() << "QWidget{}.font() = " << w->font();

                QWidget w(parent);
                w.setFont(set_bold_italic);

                // families() differs
                diff_fonts(w.font(), set_bold_italic);
            }

            QFont font_dialog;
            if (true) {
                HEADING("QFontDialog replaces bold/italic with styleName (usually Regular)")
                font_dialog = QFontDialog::getFont(nullptr, set_bold_italic);

                // Generic (correct):
                // Source Sans Pro,10,-1,5,75,1,0,0,0,0,Bold Italic
                // KDE (dialog defaults to Regular, but `f` previously contained Bold Italic):
                // Source Sans Pro,10,-1,5,50,0,0,0,0,0,Regular
                qDebug() << font_dialog.toString();
            }

            QFont from_string;
            // This is font_dialog's value, if I manually select Bold Italic in the font dialog.
            from_string.fromString("Source Sans Pro,10,-1,5,75,1,0,0,0,0,Bold Italic");

            {append_widget(QPushButton(parent));
                /*
                KDE (cancel font dialog): True-bold faux-italic.
                KDE (switch fonts/weights): True-bold, no/faux italic depending on what you pick.
                KDE (switch to font with only one weight): This call does nothing.
                */
                if (false) font_dialog.setStyleName("Bold");

                w->setText("asdfghjkl Font font_dialog");
                w->setFont(font_dialog);
            }

            {append_widget(QPushButton(parent));
                w->setText("asdfghjkl QFont::fromString(bold italic)");
                w->setFont(from_string);
            }

            {append_widget(QPushButton(parent));
                QFont regular{font_dialog};
                regular.fromString("Source Sans Pro,10,-1,5,50,0,0,0,0,0");

                w->setText("asdfghjkl QFont::fromString(no styleName) -> regular");
                w->setFont(regular);
            }

            {append_widget(QPushButton(parent));
                QFont empty_style{from_string};
                empty_style.setStyleName("");

                w->setText("asdfghjkl setStyleName(\"\")");
                w->setFont(empty_style);
            }

            QString border_red{"border: 1px solid red; padding: 4px;"};
            QString no_border{"border: unset; padding: unset;"};

            auto append_font_string = [](QLabel * w) {
                w->setText(w->text() + "\n" + w->font().toString());
            };
            auto append_resolve_mask = [](QLabel * w, QFont & f) {
                w->setText(
                    w->text() + "\n" +
                    "Resolve mask == 0x" + QString::number(get_resolve_mask(f), 16)
                );
            };

            {append_container(QWidget(parent), QVBoxLayout(w));
                w->setFont(font_dialog);
                w->setStyleSheet(border_red);

                {append_widget(QLabel("Outer QWidget is QFontDialog", parent));
                    w->setStyleSheet(no_border);
                    append_font_string(w);
                }

                QFont font_dialog_mod{font_dialog};
                font_dialog_mod.setBold(false);
                font_dialog_mod.setItalic(false);
                {append_widget(QLabel("Inner QWidget is QFontDialog + (bold=false italic=false)", parent));
                    w->setFont(font_dialog_mod);
                    append_font_string(w);
                }

                QFont new_qfont;
                {append_widget(QLabel("Inner QWidget is QFont(Source Sans Pro)", parent));
                    w->setFont(new_qfont);
                    append_font_string(w);
                    append_resolve_mask(w, font_dialog_mod);
                }

                QFont new_qfont_setAttributes{new_qfont};
                setAttributes(new_qfont_setAttributes, false, {}, false, "");
                {append_widget(QLabel("Inner QWidget is above + (bold=false italic=false styleName=\"\")", parent));
                    w->setFont(new_qfont_setAttributes);
                    append_font_string(w);
                }
            }

            append_stretch();
        }

        // Right column, dy elements
        {append_layout(QVBoxLayout());
            QFont bold{"Source Sans Pro"};
            qDebug() << bold.italic();
            setAttributes(bold, MaybeBool{}, QFont::Thin, MaybeBool{}, "");
            qDebug() << bold.italic();
            for (int x = 0; x <= 5; x++) {
                {append_widget(QPushButton(parent));
                    w->setText(
                        QString{"asdfghjkl Bolded it %1 times, got %2 weight"}
                            .arg(x).arg(QFontInfo{bold}.weight())
                    );
                    w->setFont(bold);
                    moreBold(bold);
                }
            }

            append_stretch();
        }
    }

    wv.show();
    return a.exec();
}
