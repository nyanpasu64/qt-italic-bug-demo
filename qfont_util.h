#pragma once

#include <QFont>
#include <QFontInfo>
#include <QString>

#include <type_traits>

template<typename MaybeT_, typename SomeT_>
struct Maybe {
    // Surely this would be less ugly if I set CMake to C++17 mode...
    using MaybeT = typename std::enable_if<std::is_signed<MaybeT_>::value, MaybeT_>::type;
    using SomeT = SomeT_;

    MaybeT value;

private:
    constexpr static MaybeT _noop = -1;

public:
    // implicit
    Maybe() : value(_noop) {}

    // implicit
    Maybe(MaybeT value) : value(value) {}
};


template<typename Maybe>
static bool is_set(Maybe x) {
    return x.value >= 0;
}

template<typename Maybe>
static typename Maybe::SomeT value(Maybe x) {
    return x.value;
}


struct MaybeBool : Maybe<signed char, bool> {
    using Super = Maybe<signed char, bool>;

    MaybeBool() : Super() {}
    MaybeBool(bool value) : Super(value ? True : False) {}

    constexpr static MaybeT False = 0;
    constexpr static MaybeT True = 1;
};

struct MaybeUnsigned : Maybe<int, int> {
    using Super = Maybe<int, int>;

    using Super::Super;
};


using MaybeString = QString;


/// bold/weight/italic will *optionally* modify font.
/// styleName will unconditionally be written to font.
static void setAttributes(
    QFont & font,
    MaybeBool bold,
    MaybeUnsigned weight,
    MaybeBool italic,
    MaybeString const & styleName
) {
    if (is_set(bold)) font.setBold(value(bold));
    if (is_set(weight)) font.setWeight(value(weight));
    if (is_set(italic)) font.setItalic(value(italic));
    font.setStyleName(styleName);
}


// TODO use QFontDatabase.
// Because QFontInfo thinks different QFont have different weights,
// even if they resolve to the same .ttf/otf.
static void moreBold(QFont & font) {
    int bold_weight = [&]() {
        int w = QFontInfo{font}.weight();

        // 0 => 25
        if (w <= QFont::Thin) return QFont::Light;
        // 25 => 50
        if (w <= QFont::Light) return QFont::Normal;
        // 50 => 75
        if (w <= QFont::Normal) return QFont::Bold;
        // 57 => 81
        if (w <= QFont::Medium) return QFont::ExtraBold;
        // 63 => 87
        if (w <= QFont::DemiBold) return QFont::Black;
        // 99 => 87
        return QFont::Black;
        // yeah i know the last two branches are the same.
    }();
    setAttributes(font, MaybeBool{}, MaybeUnsigned {bold_weight}, MaybeBool{}, "");
}
